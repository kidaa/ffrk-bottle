from setuptools import setup

setup(name='FFRK',
      version='0.13',
      description='An FFRK Database',
      author='rEtSaM',
      author_email='calvin@retsam-games.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=(),
)
